@extends('layouts.master')
@section('titulo')
    Crear Paquete
@endsection
@section('contenido')
    <div class="row">
        <div class="offset-md-3 col-md-6">
            <div class="card">
                <div class="card-header text-center">Añadir paquete</div>
                <form action="" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="card-body" style="padding:30px">

                        <div class="form-group">
                            <label for="direccion">Direccion de entrega</label>
                            <input type="text" name="direccion" id="direccion" class="form-control" required>
                        </div>

                        <div class="form-group">
                            <label for="fechaNacimiento">Fecha de Nacimiento</label>
                            <select name="" id="">
                                @foreach($trans as $tran)
                                    <option value="">{{$tran->nombre}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="imagen">Imagen</label>
                            <input type="file" name="imagen" id="imagen" class="form-control" required>
                        </div>
                        <div class="form-group text-center">
                            <input type="submit" name="añadir" id="añadir" class="btn btn-danger mt-3">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
