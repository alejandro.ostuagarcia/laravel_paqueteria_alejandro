@extends('layouts.master')
@section('titulo')
    Paqueteria
@endsection

@section('contenido')
    <div class="row align-items-center justify-content-center">
        @foreach( $transportistas as $transportista)
            <div class="col-3 card border shadow rounded text-center m-3 ">
                <a class="text-decoration-none p-0" href="{{ route('transportistas.show' , $transportista) }}">
                    <h4 class="text-dark" >{{$transportista->nombre}} {{$transportista->apellidos}}</h4>
                    <hr class="text-danger">
                    <img class="mb-3" src="{{asset('assets/imagenes/transportistas')}}/{{$transportista->imagen}}" width="200px" height="200px" alt="imagen"/>
                    <p class="text-dark">Paquetes: {{count($transportista->paquetes)}}

                    </p>
                </a>
            </div>
        @endforeach
    </div>
@endsection
