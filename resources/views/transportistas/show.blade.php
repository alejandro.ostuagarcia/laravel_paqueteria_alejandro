@extends('layouts.master')
@section('titulo')
    {{$transportista->nombre}}
@endsection
@section('contenido')

    <div class="row">
        <div class="col-sm-3">
            <img class="img-fluid border rounded" src="{{asset('assets/imagenes/transportistas')}}/{{$transportista->imagen}}"  alt="">
        </div>
        <div class="col-sm-9">
            <h1>{{$transportista->nombre}}</h1>
            <p>Fecha de carnet {{$transportista->fechaPermisoConducir}}</p>
            <fieldset class="row">
                <legend>Empresas</legend>
                <p class="border rounded">
                    @foreach($transportista->empresas as $empresa)
                    {{$empresa->nombre}} <br>
                @endforeach
                </p>
            </fieldset>

            <fieldset class="row">
                <legend>Paquetes</legend>
                @foreach($transportista->paquetes as $paquete)
                    <p class="border rounded">Paquete: {{$paquete->id}} - {{$paquete->direccion}}</p>
                @endforeach
            </fieldset>
{{--            <a href="{{route('transportistas.entregado')}}" class="btn btn-warning">Entregar Todo</a>--}}
{{--            <a href="{{route('transportistas.noentregado')}}" class="btn btn-warning">Marcar Todo como no entregado</a>--}}
        </div>
    </div>



@endsection
