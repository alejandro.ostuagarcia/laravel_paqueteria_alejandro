<?php

namespace App\Http\Controllers;

use App\Models\Paquete;
use App\Models\Transportista;
use Illuminate\Http\Request;

class TransportistaController extends Controller
{
    public function index(){
        $transportistas=Transportista::all();
        return view('transportistas.index',compact('transportistas'));
    }
    public function show(Transportista $transportista){

        return view('transportistas.show',compact('transportista'));
    }
    //no me dio tiempo
    public function entregado(Paquete $paquete){

        return view('transportistas.show',compact('paquete'));
    }
    public function noentregado(Paquete $paquete){

        return view('transportistas.show',compact('paquete'));
    }

}
