<?php

namespace App\Http\Controllers;

use App\Models\Paquete;
use App\Models\Transportista;
use Illuminate\Http\Request;

class PaqueteController extends Controller
{
    public function create(){
        $trans = Transportista::all();
        return view('paquetes.create', compact('trans'));
    }

    public function store(Request $request)
    {
        $p= new Paquete();
        $p->direccion = $request->direccion;
        $p->transportista_id = $request->transportista_id;
        $p->imagen = $request->imagen;
        $p->save();
    return redirect()->route('transportistas.index');
    }
}
