<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transportista extends Model
{
    use HasFactory;
    protected $table="transportista";
    public function empresas(){
        return $this->belongsToMany(Empresa::class);
    }
    public function paquetes(){

        return $this->hasMany(Paquete::class);
    }
}
