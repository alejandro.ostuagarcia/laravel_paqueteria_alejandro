<?php

namespace Database\Seeders;

use App\Models\Empresa;
use App\Models\Paquete;
use App\Models\Transportista;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        Empresa::factory(20)->create();
        $this->call(TransportistaSeeder::class);
        Paquete::factory(40)->create();
    }
}
