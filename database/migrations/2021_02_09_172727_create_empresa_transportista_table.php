<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmpresaTransportistaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empresa_transportista', function (Blueprint $table) {
            //segun la documentacion foreignId es lo mismo que unsignedBigInteger
            $table->foreignId('empresa_id');
            $table->foreignId("transportista_id");
            $table->foreign('empresa_id')->references('id')->on('empresas');
            $table->foreign('transportista_id')->references('id')->on('transportista');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('empresa_transportista');
    }
}
